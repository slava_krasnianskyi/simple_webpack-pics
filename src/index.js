require('file-loader?name=[name].[ext]!./index.html');
import "./sass/main.scss";

/**
 * Creates NodeList from array and appends it to DOM
 * @param {{}[]} photos 
 * @returns void
 */
function renderPhotos(photos=[]){
  // check  if container exists
  let allPhotos = document.querySelector('.mainPage-photos');

  if(!allPhotos){
    let targgetElem = document.querySelector('.mainPage');
    allPhotos = document.createElement('div');
    allPhotos.classList.add('mainPage-photos');
    targgetElem.appendChild(allPhotos);
  }

  for(let i = photos.length; i--;){
    // creating single photo elem
    let photoWrapper = document.createElement('div');
        photoWrapper.classList.add('mainPage-photos-item');
        photoWrapper.style.backgroundImage = `url(${photos[i].urls.small})`

    let photoAuthor = document.createElement('a');
        photoAuthor.innerText = photos[i].user.first_name
        photoAuthor.href = photos[i].user.links.html;
        photoAuthor.classList.add('mainPage-photos-item-author');
        photoAuthor.target= "_blank";
    
    photoWrapper.appendChild(photoAuthor);
    allPhotos.appendChild(photoWrapper);
  }
}

document.addEventListener('DOMContentLoaded', function() {
  const token = 'acecc0ec01da95f27a591d6980d4f7a7f056a3badf0880c9ef195c4923850dc9';
  const url = `https://api.unsplash.com/photos/?client_id=${token}`
  
  fetch(url)
  .then( resp => {
    return resp.json();
  })
  .then( r => {
    renderPhotos(r);
  } )
  .catch( e => {
    console.log('%c e ', 'color: orange; font-size: 16px; font-weight: bold; border-left: 5px solid orange', e )
  } )
});

